package com.amazon.reviewspider.es;

import com.amazon.reviewspider.ReviewspiderApplication;
import com.amazon.reviewspider.common.URLS;
import com.amazon.reviewspider.job.ReviewTask;
import com.amazon.reviewspider.model.*;
import com.amazon.reviewspider.repository.ASINHubRepository;
import com.amazon.reviewspider.repository.ReviewMentionRepository;
import com.amazon.reviewspider.repository.ReviewRepository;
import com.amazon.reviewspider.service.ASINHubService;
import com.amazon.reviewspider.service.KeyWordService;
import com.amazon.reviewspider.service.ReviewMentionService;
import com.amazon.reviewspider.service.ReviewService;
import com.amazon.reviewspider.spider.pipeline.ASINHubPipeline;
import com.amazon.reviewspider.spider.pipeline.ESPipeline;
import com.amazon.reviewspider.spider.pipeline.ReviewMentionPipeline;
import com.amazon.reviewspider.spider.process.ASINHubProcess;
import com.amazon.reviewspider.spider.process.AmazonReviewProcess;
import com.amazon.reviewspider.spider.process.ReviewMentionProcess;
import com.google.common.collect.Lists;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.StringTerms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import us.codecraft.webmagic.Spider;

import java.net.URL;
import java.util.*;
import static org.elasticsearch.index.query.QueryBuilders.matchAllQuery;
import static org.elasticsearch.index.query.QueryBuilders.matchPhraseQuery;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.elasticsearch.search.aggregations.AggregationBuilders.terms;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class ElasticsearchTemplateAggregationTests {


        //B00Q2VIW9M
        //B00005T3BG
    //B078WP9M4F

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;
    @Autowired
    private ReviewService reviewService;
    @Autowired
    private ReviewRepository reviewRepository;
    @Autowired
    private ReviewMentionRepository reviewMentionRepository;

    @Qualifier("eSPipeline")
    @Autowired
    private ESPipeline esPipeline;

    @Qualifier("reviewMentionPipeline")
    @Autowired
    private ReviewMentionPipeline reviewMentionPipeline;

    @Qualifier("asinHubPipeline")
    @Autowired
    private ASINHubPipeline asinHubPipeline;

    @Autowired
    private ASINHubRepository asinHubRepository;

    @Autowired
    private ReviewTask reviewTask;

    @Autowired
    private ASINHubService asinHubService;

    @Autowired
    private KeyWordService keyWordService;

    @Autowired
    private ReviewMentionService reviewMentionService;

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;


    @Before
    public void setMockMvc(){
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }


    @Test
    public void init(){
        List<String> asinList = Lists.newArrayList("B078WP9M4F");
        asinList.forEach(asin->{
            String reviewUrl = String.format(URLS.AMAZON_REVIEW, asin);
            String reviewMentionUrl = String.format(URLS.AMAZON_REVIEW_MENTION,asin);
            Spider.create(new ReviewMentionProcess(asin))
                    .addUrl(reviewMentionUrl)
                    .addPipeline(reviewMentionPipeline)
                    .thread(10)
                    .run();
            Spider.create(new AmazonReviewProcess(asin))
                    .addPipeline(esPipeline)
                    .addUrl(reviewUrl)
                    .thread(10)
                    .run();
            //提取关键词
            reviewTask.generateReviewExtractKeyword(asin);

        });
    }


    @Test
    public void asinHubTest(){
//        List<ReviewHubVO> allASINHub = reviewService.findAllASINHub();
//        System.out.println(allASINHub.toString());
    }

    @Test
    public void shouldReturnAggregatedResponseForGivenSearchQuery() {

        ArrayList<ReviewHubVO>  reviewHubVOS= Lists.newArrayList();
        // given
        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(matchAllQuery())
                .withSearchType(SearchType.DEFAULT)
                .withTypes("review")
                .addAggregation(terms("asin_group").field("asin"))
//                .addAggregation(terms("star_group").field("star"))
                .build();
        // when
        Aggregations aggregations = elasticsearchTemplate.query(searchQuery, SearchResponse::getAggregations);
//         then
        Map<String, Aggregation> aggregationMap = aggregations.asMap();
        for (String s : aggregationMap.keySet()) {
            StringTerms stringTerms = (StringTerms) aggregationMap.get(s);
            List<StringTerms.Bucket> buckets = stringTerms.getBuckets();
            for (Terms.Bucket bucket : buckets) {
                long docCount = bucket.getDocCount();//asin文档总数
                String asin = bucket.getKeyAsString();
                ReviewHubVO reviewHubVO = searchASINStar(docCount, asin);
                reviewHubVOS.add(reviewHubVO);
            }
        }
        System.out.println(reviewHubVOS);
    }


    private ReviewHubVO searchASINStar(Long docCount, String asin) {
        Long one = 0L, two = 0L, three = 0L, four = 0L, five = 0L;
        ReviewHubVO.ReviewHubVOBuilder hubVOBuilder = ReviewHubVO.builder();
        NativeSearchQuery starSearchQuery = new NativeSearchQueryBuilder().withQuery(matchPhraseQuery("asin", asin)).withTypes("review").addAggregation(terms("star_group").field("star")).build();
        Aggregations starAggregations = elasticsearchTemplate.query(starSearchQuery, SearchResponse::getAggregations);
        Map<String, Aggregation> starAggregationMap = starAggregations.asMap();
        for (String tmp : starAggregationMap.keySet()) {
            StringTerms starTerms = (StringTerms)starAggregationMap.get(tmp);
            List<StringTerms.Bucket> starBuckets = starTerms.getBuckets();
            for (Terms.Bucket starBucket : starBuckets) {
                String starKey = starBucket.getKeyAsString();
                long starCount = starBucket.getDocCount();
                switch (starKey) {
                    case "1.0": {
                        one = starCount;
                        break;
                    }
                    case "2.0": {
                        two = starCount;
                        break;
                    }
                    case "3.0": {
                        three = starCount;
                        break;
                    }
                    case "4.0": {
                        four = starCount;
                        break;
                    }
                    case "5.0": {
                        five = starCount;
                        break;
                    }
                }
            }
            hubVOBuilder
                    .asin(asin)
                    .reviewCount(docCount)
                    .starLevelOne(one)
                    .starLevelTwo(two)
                    .starLevelThree(three)
                    .starLevelFour(four)
                    .starLevelFive(five);
        }
        return hubVOBuilder.build();
    }


    @Test
    public void searchaReview(){
        Integer pageNumber = 1;
        String asin = "B00005T3BG";
        String keyword = "good";
        Float star = 5F;
        ASINReviewModel build = ASINReviewModel.builder()
                .asin(asin)
                .terms(keyword)
                .star(star)
                .pageNumber(pageNumber)
                .build();
        PageModel search = reviewService.search(build);
    }


    @Test
    public void getAsinReviewMentionWords(){
        String asin = "B00005T3BG";
        MatchQueryBuilder matchQuery = QueryBuilders.matchQuery("asin", asin);
        Iterable<ReviewMention> search = reviewMentionRepository.search(matchQuery);
        System.out.println(search.iterator().hasNext());

    }


    @Test
    public void findByASINTest(){

        String asin = "B00005T3BG";



        // review
        Iterable<Review> iterable = reviewService.getAllByASIN(asin);
        for (Review anIterable : iterable) {
            System.out.println(anIterable);
        }

        System.out.println("==========================");
        //asin
        Iterator<ASINHub> asinHubIterator = asinHubService.findByasin(asin);
        while (asinHubIterator.hasNext()){
            System.out.println(asinHubIterator.next());
        }
        System.out.println("==========================");

        //keyword
        Iterable<KeyWordDIY> keyWord = keyWordService.getKeyWord(asin);
        for (KeyWordDIY aKeyWord : keyWord) {
            System.out.println(aKeyWord);
        }
        System.out.println("==========================");
        //reviewMention
        Iterable<ReviewMention> reviewMentionIterable = reviewMentionService.findByASIN(asin);
        Iterator<ReviewMention> reviewMentionIterator = reviewMentionIterable.iterator();
        while (reviewMentionIterator.hasNext()){
            System.out.println(reviewMentionIterator.next());
        }
    }

    @Test
    public void asinHubDetailTest(){
        String url = String.format(URLS.AMAZON_ASIN_DETAIL,"B00Q2VIW9M");
        Spider.create(new ASINHubProcess("B00Q2VIW9M","US"))
                .addUrl(url)
                .addPipeline(asinHubPipeline)
                .thread(5)
                .run();
    }

    @Test
    public void updateFidleFileData() throws Exception {
        String indexName = "asin_idx",type="asin";
        String url = String.format("http://localhost:9200/%s/_mapping/%s", indexName, type);
        MvcResult acknowledged = mockMvc.perform(MockMvcRequestBuilders.put(url)
                .contentType(MediaType.APPLICATION_JSON).content("{\"properties\": {\"asin\": { \"type\": \"text\",\"fielddata\": true}}}"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("acknowledged").value(true)).andReturn();
        System.out.println(acknowledged.getResponse().getContentAsString());
    }

    @Test
    public void findASINHubByASIN(){

        String term = "*B00*";
        BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
        queryBuilder.should(QueryBuilders.wildcardQuery("asin.keyword", term));
        queryBuilder.should(QueryBuilders.wildcardQuery("brand", term));
        for (ASINHub next : asinHubRepository.search(queryBuilder)) {
            System.out.println(next);
        }

        //        List<ReviewHubVO> asinHub = reviewService.findAllASINHub("B00");
//        System.out.println(asinHub.size());
//        System.out.println(asinHub.toString());



    }
}




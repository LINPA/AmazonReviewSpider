package com.amazon.reviewspider.spider.process;

import com.amazon.reviewspider.spider.pipeline.KwRankPipeline;
import com.google.common.collect.Maps;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Html;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class KwRankProcess implements PageProcessor {


    private String target;

    private boolean sponsoredFlag = false;
    private boolean originFlag = false;

    private int sponsoredIdx = -1;
    private int originIdx = -1;

    private Site site = Site.me().setRetryTimes(3).setSleepTime(1000).setUserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36");;

    private AtomicInteger currentPage = new AtomicInteger(1);


    private KwRankProcess(String target){
        this.target = target;
    }

    @Override
    public void process(Page page) {
        Html html = page.getHtml();
        String prefix = "//body//div[@id='a-page']//div[@id='search-main-wrapper']//div[@id='searchTemplate']//div[@id='rightContainerATF']//div[@id='rightResultsATF']//div[@id='resultsCol']";
        List<String> sponsoredAsin = html.xpath(prefix+
                        "//div[@id='centerMinus']" +
                        "//div[@id='atfResults']" +
                        "//ul[@id='s-results-list-atf']//li/@data-asin").all();
        List<String> originAsin = html.xpath(prefix+
                "//div[@id='centerBelowPlus']" +
                "//div[@id='btfResults']" +
                "//ul//li/@data-asin").all();
        System.out.println("当前页："+currentPage);
        System.out.println("输出当前页的ASIN数据：");
        System.out.println("sponsoredAsin "+sponsoredAsin);
        System.out.println("originAsin "+originAsin);

//        sponsoredAsin.addAll(originAsin);
//        sponsoredAsin.removeAll(Arrays.asList("",null));



        //检查是否在当前页可以找到

        //sponsored
        if (!sponsoredFlag){
            boolean contains = sponsoredAsin.contains(target);
            if (contains){
                sponsoredFlag = true;
                sponsoredIdx = currentPage.get();
            }
        }
        //origin
        if (!originFlag){
            boolean contains = originAsin.contains(target);
            if (contains){
                originFlag = true;
                originIdx = currentPage.get();
            }
        }

        if (sponsoredFlag && originFlag){
            LinkedHashMap<String, String> location = Maps.newLinkedHashMap();
            location.put("asin",target);
            location.put("sponsored",String.valueOf(sponsoredIdx));
            location.put("origin",String.valueOf(originIdx));
            page.putField("location",location);
        }

        String next = page.getHtml().xpath("//body" +
                "//div[@id='a-page']" +
                "//div[@id='search-main-wrapper']" +
                "//div[@id='main']" +
                "//div[@id='searchTemplate']" +
                "//div[@id='rightContainerATF']" +
                "//div[@id='rightResultsATF']" +
                "//div[@id='centerBelowMinus']" +
                "//div[@id='bottomBar']" +
                "//div[@id='pagn']" +
                "//span[@class='pagnRA']//a/@href").toString();

        if (null != next && !sponsoredFlag && !originFlag) {
            page.addTargetRequest(next);
            currentPage.getAndIncrement();
        }else if (sponsoredFlag && originFlag){
            LinkedHashMap<String, String> location = Maps.newLinkedHashMap();
            location.put("asin",target);
            location.put("sponsored",String.valueOf(sponsoredIdx));
            location.put("origin",String.valueOf(originIdx));
            page.putField("location",location);
        } else {
            LinkedHashMap<String, String> location = Maps.newLinkedHashMap();
            location.put("asin",target);
            location.put("sponsored",String.valueOf(sponsoredIdx));
            location.put("origin",String.valueOf(originIdx));
            page.putField("location",location);
        }


    }

    @Override
    public Site getSite() {
        return site;
    }


    private static void keywordSearch(String term, String target){
        StringBuilder sb = new StringBuilder();
        if (term != null){
            String[] words = term.split(" ");
            if (words.length >= 1) sb.append(words[0]);
            for (int i =1;i<words.length;i++){
                sb.append("+").append(words[i]);
            }
        }
        String reviewUrl = String.format("https://www.amazon.com/s/ref=nb_sb_noss_1?url=search-alias=aps&field-keywords=%s", sb.toString());
        Spider.create(new KwRankProcess(target))
                .addPipeline(new KwRankPipeline())
                .addUrl(reviewUrl)
                .thread(3)
                .run();
    }


    public static void main(String[] args) {
        keywordSearch("wireless bluetooth headphones on ear","B07CGSRF69");

    }
}

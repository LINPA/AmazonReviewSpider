package com.amazon.reviewspider.spider.pipeline;

import com.amazon.reviewspider.model.Review;
import com.amazon.reviewspider.model.ReviewMention;
import com.amazon.reviewspider.service.ReviewMentionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;


@Component("reviewMentionPipeline")
public class ReviewMentionPipeline implements Pipeline {


    @Autowired
    private ReviewMentionService reviewMentionService;

    @Override
    public void process(ResultItems resultItems, Task task) {
        String url = resultItems.getRequest().getUrl();
        System.out.println("get from "+url);
        ReviewMention reviewMention = resultItems.get("reviewMention");
        reviewMentionService.save(reviewMention);
    }


}

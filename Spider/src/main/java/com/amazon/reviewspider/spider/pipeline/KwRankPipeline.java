package com.amazon.reviewspider.spider.pipeline;

import org.springframework.stereotype.Component;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

import java.util.LinkedHashMap;
import java.util.List;


@Component("kwRankPipeline")
public class KwRankPipeline implements Pipeline {
    @Override
    public void process(ResultItems resultItems, Task task) {
        LinkedHashMap<String, String> location = resultItems.get("location");
        System.out.println("=======================");
        System.out.println("asin : "+location.get("asin"));
        System.out.println("sponsored : "+location.get("sponsored"));
        System.out.println("origin : "+location.get("origin"));
        System.out.println("=======================");
    }
}

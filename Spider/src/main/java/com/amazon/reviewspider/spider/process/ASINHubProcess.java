package com.amazon.reviewspider.spider.process;

import com.amazon.reviewspider.common.SnowFlake;
import com.amazon.reviewspider.model.ASINHub;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Selectable;

public class ASINHubProcess implements PageProcessor {

    private String asin;

    private String region;

    private Object whos;

    private Site site = Site.me().setRetryTimes(3).setSleepTime(0);

    private SnowFlake snowFlake = new SnowFlake(2,0);

    public ASINHubProcess(String asin, String region){
        this.asin = asin;
        this.region = region;
    }


    private static String DEFAULT_PATH = "//body//div[@id='a-page']//div[@id='dp']//div[@id='dp-container']";

    @Override
    public void process(Page page) {

        //star
        String starStr = page.getHtml().xpath(DEFAULT_PATH +
                "//div[@id='centerCol']" +
                "//div[@id='averageCustomerReviews_feature_div']" +
                "//div[@id='averageCustomerReviews']" +
                "//span[@class='a-declarative']" +
                "//span[@id='acrPopover']" +
                "//span[@class='a-declarative']" +
                "//a//i//span/text()").toString();

        String[] starStrSplit = starStr.split(" ");
        Float star = Float.valueOf(starStrSplit[0]);
        //brand
        String brand = page.getHtml().xpath(DEFAULT_PATH +
                "//div[@id='centerCol']" +
                "//div[@id='bylineInfo_feature_div']" +
                "//div[@class='a-section a-spacing-none']" +
                "//a/text()").toString();
        //img
        String img = page.getHtml().xpath(DEFAULT_PATH +
                "//div[@id='leftCol']" +
                "//div[@id='imageBlock_feature_div']" +
                "//div[@id='imageBlock']" +
                "//div[@class='a-fixed-left-grid']" +
                "//div[@class='a-fixed-left-grid-inner']" +
                "//div[2]" +
                "//div[1]" +
                "//div[@id='main-image-container']" +
                "//ul" +
                "//li[1]" +
                "//span[@class='a-list-item']" +
                "//span[@class='a-declarative']" +
                "//div[@id='imgTagWrapperId']" +
                "//img/@data-old-hires").toString();
        ASINHub.ASINHubBuilder asinHubBuilder = ASINHub.builder()
                .id(snowFlake.nextId())
                .asin(asin)
                .starLevel(star)
                .brand(brand)
                .country(region)
                .img(img)
                .url(page.getUrl().toString());
        page.putField("asinHub",asinHubBuilder.build());
    }

    @Override
    public Site getSite() {
        return site;
    }


    public String getAsin() {
        return asin;
    }

    public void setAsin(String asin) {
        this.asin = asin;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Object getWhos() {
        return whos;
    }

    public void setWhos(Object whos) {
        this.whos = whos;
    }
}

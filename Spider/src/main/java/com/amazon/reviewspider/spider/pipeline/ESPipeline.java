package com.amazon.reviewspider.spider.pipeline;

import com.amazon.reviewspider.model.Review;
import com.amazon.reviewspider.service.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.PageModelPipeline;
import us.codecraft.webmagic.pipeline.Pipeline;

import java.util.concurrent.CopyOnWriteArrayList;

@Component("eSPipeline")
public class ESPipeline implements Pipeline {

    @Autowired
    private ReviewService reviewService;

    @Override
    public void process(ResultItems resultItems, Task task) {
        String url = resultItems.getRequest().getUrl();
        CopyOnWriteArrayList<Review> reviews = resultItems.get("review");
//        reviews.forEach(review -> reviewService.saveOne(review));
        reviewService.saveAll(reviews);
    }
}

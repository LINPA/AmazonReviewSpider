package com.amazon.reviewspider.spider.pipeline;

import com.amazon.reviewspider.model.ASINHub;
import com.amazon.reviewspider.service.ASINHubService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

@Component("asinHubPipeline")
public class ASINHubPipeline implements Pipeline {


    @Autowired
    private ASINHubService asinHubService;

    @Override
    public void process(ResultItems resultItems, Task task) {
      ASINHub asinHub = resultItems.get("asinHub");
      asinHubService.save(asinHub);
    }
}

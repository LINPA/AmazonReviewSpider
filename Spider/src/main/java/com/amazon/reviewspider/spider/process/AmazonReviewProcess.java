package com.amazon.reviewspider.spider.process;

import com.amazon.reviewspider.common.DateUtils;
import com.amazon.reviewspider.common.SnowFlake;
import com.amazon.reviewspider.common.URLS;
import com.amazon.reviewspider.model.Review;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Html;
import us.codecraft.webmagic.selector.Selectable;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.IntStream;

public class AmazonReviewProcess implements PageProcessor {

    int x= 0;

    private String asin;

    private Site site = Site.me().setRetryTimes(3).setSleepTime(0);

    private SnowFlake snowFlake = new SnowFlake(0,0);

    private final String DEAFULT_URL = "//body//div[@id='a-page']/div/div[@class='a-section a-spacing-small page-content page-min-width']//div[@class='a-fixed-right-grid view-point']//div[@class='a-fixed-right-grid-inner']//div[@class='a-fixed-right-grid-col a-col-left']//div[@class='a-section a-spacing-none reviews-content a-size-base']//div[@id='cm_cr-review_list']";
    private  String DEFAULT_PAGE = "/product-reviews/%s/ref=cm_cr_othr_d_paging_btm_1?ie=UTF8&reviewerType=all_reviews&pageNumber=";


    public AmazonReviewProcess(String asin) {
        this.asin = asin;
    }

    @Override
    public void process(Page page) {
        Html html = page.getHtml();
        List<String> all = html.xpath(DEAFULT_URL+"//div[@class='a-section review']").all();
        CopyOnWriteArrayList<Review> reviews = new CopyOnWriteArrayList<>();
        if (all != null){
            for (String s : all){
                Html tempHtml = Html.create(s);
                //star
                String stars = tempHtml.xpath(
                        "//div[@class='a-section celwidget']" +
                        "//div[1]" +
                        "//a[1]" +
                        "//i" +
                        "//span/text()").toString();
                //review title
                String reviewTitle = tempHtml.xpath(
                        "div[@class='a-section celwidget']" +
                        "//div[1]" +
                        "//a[2]/text()").toString();
                //buyer's name
                String reviewAuthor = tempHtml.xpath(
                        "div[@class='a-section celwidget']" +
                        "//div[2]//span[1]" +
                        "//a/text()").toString();
                //date
                String reviewDate = tempHtml.xpath(
                        "div[@class='a-section celwidget']" +
                        "//div[2]" +
                        "//span[4]/text()").toString();
                //review body
                String reviewBody = tempHtml.xpath(
                        "div[@class='a-section celwidget']" +
                        "//div[4]" +
                        "//span/text()").toString();

                List<String> imagesList = tempHtml.xpath(
                        "div[@class='a-section celwidget']" +
                                "//div[@class='a-section a-spacing-medium review-image-container']" +
                                "//div//img/@src").all();
                String video = tempHtml.xpath(
                        "div[@class='a-section celwidget']" +
                                "//div[@class='a-row a-spacing-medium review-data']" +
                                "//span" +
                                "//div" +
                                "//div" +
                                "//div" +
                                "//video/@src").toString();

                float star = Float.parseFloat(stars.split(" ")[0]);
                Review review = Review.builder()
                        .id(snowFlake.nextId())
                        .starLevel(star)
                        .reviewTitle(reviewTitle)
                        .review(reviewBody)
                        .buyer(reviewAuthor)
                        .asin(asin)
                        .imageUrl(imagesList)
                        .videoUrl(video)
                        .date(DateUtils.extractDate(reviewDate).getTime())
                        .build();
                reviews.add(review);
            }
            page.putField("review",reviews);
        }
//        发现链接添加到抓取队列
        List<String> list = html.xpath(DEAFULT_URL +
                "//div[@class='a-form-actions a-spacing-top-extra-large']" +
                "//span" +
                "//div[@id='cm_cr-pagination_bar']" +
                "//ul" +
                "//li").all();
        String currentUrl = page.getUrl().toString();
        boolean firstPage = currentUrl.contains("pageNumber=1");
        if (firstPage && list.size() >= 4){
            String lastPageString = list.get(list.size()-2);
            Integer totalPage = Integer.valueOf(Html.create(lastPageString).xpath("//a/text()").toString())+1;
            String format = String.format(DEFAULT_PAGE, asin);
            IntStream.range(2,totalPage).forEach(i->{
                        String tmpUrl = format+i;
                        page.addTargetRequest(tmpUrl);
                    }
            );
        }
    }

    @Override
    public Site getSite() {
        return site;
    }

//          B00Q2VIW9M
//          B00005T3BG
//          B06W55L51Q
    public static void main(String[] args) {

        String reviewUrl = String.format(URLS.AMAZON_REVIEW, "B06W55L51Q");

        Spider.create(new AmazonReviewProcess("B06W55L51Q"))
                .addUrl(reviewUrl)
                .thread(5)
                .run();

    }

    public String getAsin() {
        return asin;
    }

    public void setAsin(String asin) {
        this.asin = asin;
    }
}

package com.amazon.reviewspider.spider.process;

import com.amazon.reviewspider.common.SnowFlake;
import com.amazon.reviewspider.model.ReviewMention;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Html;

import java.util.ArrayList;
import java.util.List;

public class ReviewMentionProcess implements PageProcessor {

    private String asin;

    private Site site = Site.me().setRetryTimes(3).setSleepTime(0);

    public ReviewMentionProcess(String asin) {
        this.asin = asin;
    }

    private SnowFlake snowFlake = new SnowFlake(1,0);

    @Override
    public void process(Page page) {
        List<String> reviewsMention = new ArrayList<>();
        List<String> all = page.getHtml().xpath(
                        "//body" +
                        "//div[@id='a-page']" +
                        "//div[@id='dp']" +
                        "//div[@class='a-container']" +
                        "//div[@class='a-row a-spacing-extra-large']" +
                        "//div[@class='a-column a-span8']" +
                        "//div[@id='cr-dp-desktop-lighthut']" +
                        "//div[@id='cr-dp-lighthut']" +
                        "//div[@class='a-row']" +
                        "//div[@class='a-row cr-lighthut']" +
                        "//div[@class='cr-lighthouse-terms']" +
                        "//span[@class='a-declarative']").all();
        for (String tmp : all){
            Html html = Html.create(tmp);
            String mentionWord = html.xpath("//a//span/text()").toString();
            reviewsMention.add(mentionWord);

        }
        ReviewMention build = ReviewMention.builder()
                .id(snowFlake.nextId())
                .asin(asin)
                .word(reviewsMention).build();
        page.putField("reviewMention",build);

    }

    @Override
    public Site getSite() {
        return site;
    }

    public String getAsin() {
        return asin;
    }

    public void setAsin(String asin) {
        this.asin = asin;
    }

}

package com.amazon.reviewspider.web;

import com.amazon.reviewspider.common.ResultBean;
import com.amazon.reviewspider.service.NegativeWordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/negative")
public class NegativeWordController {

    @Autowired
    private NegativeWordService negativeWordService;


    @GetMapping
    public ResultBean allNegativeWords(){
        Set<String> words = negativeWordService.find().getWords();
        return ResultBean.success(words);
    }


    @PostMapping()
    public void negative(@RequestBody List<String> words){
        negativeWordService.updateNegativeWord(words);
    }

}

package com.amazon.reviewspider.web;

import com.amazon.reviewspider.common.ResultBean;
import com.amazon.reviewspider.model.ASINHub;
import com.amazon.reviewspider.model.ASINReviewModel;
import com.amazon.reviewspider.model.PageModel;
import com.amazon.reviewspider.model.ReviewHubVO;
import com.amazon.reviewspider.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@RestController
@RequestMapping("/hub/asin")
public class ASINController {


    @Autowired
    private ReviewService reviewService;
    @Autowired
    private RunnerService runnerService;

    // ASIN Hub 所有抓取过的asin
    @GetMapping("/term")
    public ResultBean asinHub(@Param("term") String term, HttpSession session){
        //当前操作者
        Object who = session.getAttribute("who");
        String whos = "pub";
        if (null != who){
            whos = session.toString();
        }
        List<ReviewHubVO> allASINHub = reviewService.findAllASINHub(term,whos);
        return ResultBean.success(allASINHub);
    }

    @GetMapping("/{asin}/star/{star}")
    public ResultBean asinReview(@PathVariable(value = "asin") String asin,
                                 @PathVariable(value = "star") String star){
        ASINReviewModel.ASINReviewModelBuilder builder = ASINReviewModel.builder();

        if (null != asin && !(asin.equals("none"))){
            builder.asin(asin);
        }
        if (null != star && !(star.equals("0"))){
            builder.star(star);
        }
        PageModel pageModel = reviewService.search(builder.build());
        return ResultBean.success(pageModel);
    }

    @PutMapping("/{asin}")
    public ResultBean update(@PathVariable(value = "asin") String asin) throws ExecutionException, InterruptedException {
        Future<Boolean> cleanAndRun = runnerService.cleanAndRun(asin);
        if (null != cleanAndRun){
            return ResultBean.success(cleanAndRun.get());
        }else {
            return ResultBean.fail("该ASIN值可能已经存在，请在ASIN Hub中确认后重试");
        }
    }

}

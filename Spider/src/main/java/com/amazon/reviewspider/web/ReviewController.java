package com.amazon.reviewspider.web;

import com.amazon.reviewspider.model.ASINReviewModel;
import com.amazon.reviewspider.model.PageModel;
import com.amazon.reviewspider.service.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hub/review")
public class ReviewController {

    @Autowired
    private ReviewService reviewService;

    /**
     * 初始数据和搜索共用接口
     * @param asin
     * @param star
     * @param term
     * @param pageNumber
     * @return
     */
    @GetMapping
    public PageModel allReviews(@Param("asin") String asin,
                                @Param("star") String star,
                                @Param("term") String term,
                                @Param("pageNumber") String pageNumber,
                                @Param("reviewbodyfilter") Integer type){
        ASINReviewModel.ASINReviewModelBuilder builder = ASINReviewModel.builder();
        if (null != asin && !(asin.equals("none"))){
            builder.asin(asin.toUpperCase());
        }
        if (null != star && !(star.equals("0"))){
            builder.star(star);
        }
        if (null != term && !(term.equals("none"))){
            builder.terms(term);
        }
        if (null != pageNumber){
            builder.pageNumber(Integer.valueOf(pageNumber)-1);
        }
        if (null != type){
            builder.type(type);
        }
        return reviewService.search(builder.build());
    }
}

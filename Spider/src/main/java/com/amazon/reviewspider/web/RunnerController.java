package com.amazon.reviewspider.web;

import com.amazon.reviewspider.common.ResultBean;
import com.amazon.reviewspider.common.URLS;
import com.amazon.reviewspider.job.ReviewTask;
import com.amazon.reviewspider.model.IndexDTO;
import com.amazon.reviewspider.model.Review;
import com.amazon.reviewspider.service.ReviewService;
import com.amazon.reviewspider.service.RunnerService;
import com.amazon.reviewspider.spider.pipeline.ASINHubPipeline;
import com.amazon.reviewspider.spider.pipeline.ESPipeline;
import com.amazon.reviewspider.spider.pipeline.ReviewMentionPipeline;
import com.amazon.reviewspider.spider.process.ASINHubProcess;
import com.amazon.reviewspider.spider.process.AmazonReviewProcess;
import com.amazon.reviewspider.spider.process.ReviewMentionProcess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;
import us.codecraft.webmagic.Spider;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@RestController
@RequestMapping("/hub/run")
public class RunnerController {


    @Qualifier("eSPipeline")
    @Autowired
    private ESPipeline esPipeline;

    @Qualifier("reviewMentionPipeline")
    @Autowired
    private ReviewMentionPipeline reviewMentionPipeline;

    @Qualifier("asinHubPipeline")
    @Autowired
    private ASINHubPipeline asinHubPipeline;

    @Autowired
    private ReviewService reviewService;

    @Autowired
    private ReviewTask reviewTask;

    @Autowired
    private RunnerService runnerService;

    //单个asin抓取数据,同步相应
    @PostMapping("/asin")
    public ResultBean reviewIndexSAsin(@Param("asin") String asin,HttpSession session) throws ExecutionException, InterruptedException {

        Future<Boolean> run = runnerService.run(asin);
        if (null != run){
            return ResultBean.success(run.get());
        }else {
            return ResultBean.fail("该ASIN值可能已经存在，请在ASIN Hub中确认后重试");
        }

//        Page<Review> byASIN = reviewService.findByASIN(asin);
//        if(!byASIN.iterator().hasNext()) {
//            String reviewUrl = String.format(URLS.AMAZON_REVIEW, asin);
//            String reviewMentionUrl = String.format(URLS.AMAZON_REVIEW_MENTION, asin);
//            String asinUrl = String.format(URLS.AMAZON_ASIN_DETAIL,asin);
//            Spider.create(new ASINHubProcess(asin,"US"))
//                    .addUrl(asinUrl)
//                    .addPipeline(asinHubPipeline)
//                    .thread(2)
//                    .run();
//            Spider.create(new ReviewMentionProcess(asin))
//                    .addUrl(reviewMentionUrl)
//                    .addPipeline(reviewMentionPipeline)
//                    .thread(10)
//                    .run();
//            Spider.create(new AmazonReviewProcess(asin))
//                    .addPipeline(esPipeline)
//                    .addUrl(reviewUrl)
//                    .thread(10)
//                    .run();
////            提取关键词
//            Future<Boolean> booleanFuture = reviewTask.generateReviewExtractKeyword(asin);
//            return ResultBean.success(booleanFuture.get());
//        }
//        return ResultBean.fail("该ASIN值可能已经存在，请在ASIN Hub中确认后重试");
    }

    //按照asin批量抓取数据
    @PostMapping()
    public ResultBean reviewIndex(@RequestBody IndexDTO indexDTO,HttpSession session){
        //当前操作者
        Object who = session.getAttribute("who");
        if (indexDTO.getAsin().size() < 1){
            return ResultBean.fail("asin为空");
        }
        List<String> asinList = indexDTO.getAsin();
        asinList.forEach(asin->{
            runnerService.run(asin);
//            String reviewUrl = String.format(URLS.AMAZON_REVIEW, asin);
//            String reviewMentionUrl = String.format(URLS.AMAZON_REVIEW_MENTION,asin);
//            String asinUrl = String.format(URLS.AMAZON_ASIN_DETAIL,asin);
//            Spider.create(new ASINHubProcess(asin,"US"))
//                    .addUrl(asinUrl)
//                    .addPipeline(asinHubPipeline)
//                    .thread(2)
//                    .start();
//            Spider.create(new ReviewMentionProcess(asin))
//                    .addUrl(reviewMentionUrl)
//                    .addPipeline(reviewMentionPipeline)
//                    .thread(5)
//                    .start();
//            Spider.create(new AmazonReviewProcess(asin))
//                    .addPipeline(esPipeline)
//                    .addUrl(reviewUrl)
//                    .thread(5)
//                    .start();
//            //提取关键词
////            reviewTask.generateReviewExtractKeyword(asin);

        });
        return ResultBean.success();
    }

    //true 存在
    //false 不存在
    @GetMapping("/execute")
    public Boolean findAsin(@Param("asin") String asin){
        Boolean result = Boolean.FALSE;
        Page<Review> byASIN = reviewService.findByASIN(asin);
        if(!byASIN.iterator().hasNext()) {
            result = Boolean.TRUE;
        }
        return result;
    }
}

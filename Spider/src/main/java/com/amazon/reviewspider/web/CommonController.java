package com.amazon.reviewspider.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class CommonController {

    @RequestMapping("/")
    public String index(HttpServletRequest request,Model model){

        return "index";

    }

}

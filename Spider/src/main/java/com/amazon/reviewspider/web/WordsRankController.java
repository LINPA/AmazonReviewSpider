package com.amazon.reviewspider.web;

import com.amazon.reviewspider.common.ResultBean;
import com.hankcs.hanlp.summary.TextRankKeyword;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@Controller
@RestController
@RequestMapping("/hub/terms")
public class WordsRankController {

    static TextRankKeyword textRankKeyword;
    static {
        textRankKeyword = new TextRankKeyword();
    }



    @PostMapping("/rank")
    public ResultBean termsAndRank(@RequestBody String terms){
        Map<String, Float> termAndRank = textRankKeyword.getTermAndRank(terms);
        return ResultBean.success(termAndRank);
    }
}

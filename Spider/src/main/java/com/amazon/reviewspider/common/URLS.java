package com.amazon.reviewspider.common;

public class URLS {

    public static String AMZ_US = "https://www.amazon.com/";
    public static String AMZ_UK = "https://www.amazon.co.uk/";

    public static String AMAZON_REVIEW = "https://www.amazon.com/product-reviews/%s?ie=UTF8&reviewerType=all_reviews&pageNumber=1";
    public static String AMAZON_REVIEW_MENTION = "https://www.amazon.com/dp/%s/ref=cm_cr_arp_d_bdcrb_top?ie=UTF8#customerReviews";

    public static String AMAZON_ASIN_DETAIL = "https://www.amazon.com/dp/%s";

    public static String UK_AMAZON_ASIN_DETAIL = "https://www.amazon.co.uk/gp/product/%s";
    public static String UK_AMAZON_REVIEW = "https://www.amazon.co.uk/product-reviews/%s?ie=UTF8&reviewerType=all_reviews&pageNumber=1";
}

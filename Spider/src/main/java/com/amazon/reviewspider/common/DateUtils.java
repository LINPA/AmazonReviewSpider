package com.amazon.reviewspider.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtils {

    private static final Locale DEFAULT_LOCALE = Locale.US;
    private static final SimpleDateFormat SIMPLE_DATE_FORMATE = new SimpleDateFormat("yyyy-MM-dd");


    public static Date getDate(String time) {
        try {
            return SIMPLE_DATE_FORMATE.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static Date extractDate(String dateStr){
        String substring = dateStr.substring(2);
        System.out.println("时间字符串:"+substring);
        String[] dateString = substring.split(" ");
//        System.out.println(dateString[0]);
//        System.out.println(dateString[1]);
//        System.out.println(dateString[2]);
        Integer month = whichMonth(dateString[1]);
        Integer day = Integer.valueOf(((dateString[2]).split(","))[0]);
        Integer year = Integer.valueOf(dateString[3]);
        return new Date(year, month, day);
    }


    private static Integer whichMonth(String month){
        Integer mon = 1;
        switch (month){
            case "January":
                mon = 1;
                break;
            case "February":
                mon = 2;
                break;
            case "March":
                mon = 3;
                break;
            case "April":
                mon = 4;
                break;
            case "May":
                mon = 5;
                break;
            case "June":
                mon = 6;
                break;
            case "July":
                mon = 7;
                break;
            case "August":
                mon = 8;
                break;
            case "September":
                mon = 9;
                break;
            case "October":
                mon = 10;
                break;
            case "November":
                mon = 11;
                break;
            case "December":
                mon = 12;
                break;
        }
        return mon;
    }

    public static void main(String[] args) {
        String s = "on April 22, 2018";
        Date date = extractDate(s);
        System.out.println(date.getTime());
        String s2 = "on April 24, 2018";
        Date date2 = extractDate(s2);
        System.out.println(date2.getTime());
    }

}

package com.amazon.reviewspider.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.omg.CORBA.PUBLIC_MEMBER;


@Data
@NoArgsConstructor
public class ResultBean {

    private Integer code;

    private Object data;

    private String message;

    public ResultBean(Integer code, Object data, String message) {
        this.code = code;
        this.data = data;
        this.message = message;
    }

    public static ResultBean success(Object data){
        return new ResultBean(200,data,"OK");
    }

    public static ResultBean success(){
        return new ResultBean(200,null,"OK");
    }

    public static ResultBean fail(String message){
        return new ResultBean(400,null,message);
    }
}

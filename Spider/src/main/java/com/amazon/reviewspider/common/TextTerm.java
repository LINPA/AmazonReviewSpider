package com.amazon.reviewspider.common;

import com.google.common.collect.Lists;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.core.StopAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.*;

public class TextTerm {

    static Analyzer analyzer = new StopAnalyzer();


    public static List<Map.Entry<String, Integer>> generateTermsCount(List<String> documents) throws IOException {
        ArrayList<String> keywords = Lists.newArrayList();
        Hashtable<String, Integer> hashtable= new Hashtable<>();
        for (String document : documents) {
            Reader r = new StringReader(document);
            TokenStream tokenStream = analyzer.tokenStream("", r);
            CharTermAttribute charTermAttribute = tokenStream.addAttribute(CharTermAttribute.class);
            tokenStream.reset();
            while (tokenStream.incrementToken()) {
                keywords.add(charTermAttribute.toString());
            }
        }
        for (String term : keywords) {
            if (!hashtable.containsKey(term)){
                hashtable.put(term, 1);
            }else {
                hashtable.put(term, hashtable.get(term) +1);
            }
        }
        List<Map.Entry<String, Integer>> list = new ArrayList<>(hashtable.entrySet());
        //降序排序
        list.sort((o1, o2) -> {
            //升序：return o1.getValue().compareTo(o2.getValue());
            return o2.getValue().compareTo(o1.getValue());
        });
        return list;
    }


    public static ArrayList<String> getKeyword(String document) throws IOException {
        ArrayList<String> keywords = Lists.newArrayList();
        Reader r = new StringReader(document);
        TokenStream tokenStream = analyzer.tokenStream("", r);
        CharTermAttribute charTermAttribute = tokenStream.addAttribute(CharTermAttribute.class);
        tokenStream.reset();
        while (tokenStream.incrementToken()) {
           keywords.add(charTermAttribute.toString());
        }
        return keywords;
    }


    public static List<Map.Entry<String, Integer>> termWordCount(List<String> keywords){
        Hashtable<String, Integer> hashtable= new Hashtable<>();
        for (String term : keywords) {
            if (!hashtable.containsKey(term)){
                hashtable.put(term, 1);
            }else {
                hashtable.put(term, hashtable.get(term) +1);
            }
        }
        List<Map.Entry<String, Integer>> list = new ArrayList<>(hashtable.entrySet());
        //降序排序
        list.sort((o1, o2) -> {
            //升序：return o1.getValue().compareTo(o2.getValue());
            return o2.getValue().compareTo(o1.getValue());
        });
        return list;
    }



    public static void main(String[] args) throws IOException {
        ArrayList<String> keyword = getKeyword("Coobar Video Baby Monitor with extra long range,Adjustable Lens, Infrared Night Vision Visibility,Two-Way Talk Back,VOX,LCD Display,long battery life and Portable for parents");
        List<Map.Entry<String, Integer>> entries = termWordCount(keyword);
        //输出结果
        for (Map.Entry<String, Integer> mapping : entries) {
            System.out.println(mapping.getKey() + ":\t" + mapping.getValue());
        }
    }

}

package com.amazon.reviewspider;

import com.amazon.reviewspider.model.Review;
import com.amazon.reviewspider.service.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;

@EnableAsync
@SpringBootApplication
public class ReviewspiderApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReviewspiderApplication.class, args);
	}

	@Bean
	public AsyncTaskExecutor asyncTaskExecutor(){
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setThreadNamePrefix("task-review");
		executor.setCorePoolSize(10);
		executor.setMaxPoolSize(20);
		executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
		 return executor;
	}
}

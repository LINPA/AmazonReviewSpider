package com.amazon.reviewspider.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PageModel {
    private Object content;
    private Integer limit;
    private Integer offset;
    private Long total;
    private String search;
    private String sort;
    private String order;

}

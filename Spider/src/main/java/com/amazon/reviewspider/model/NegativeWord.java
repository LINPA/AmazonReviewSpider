package com.amazon.reviewspider.model;

import lombok.*;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.List;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Document(indexName = "negativeword_idx",type = "negativeWord")
public class NegativeWord {

    private Long id;

    private Set<String> words;
}

package com.amazon.reviewspider.model;

import lombok.*;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ReviewHubVO {

    private String asin;
    private Long reviewCount;
    private String country;
    private String brand;
    private String img;
    private Long starLevelOne;
    private Long starLevelTwo;
    private Long starLevelThree;
    private Long starLevelFour;
    private Long starLevelFive;
    private List<String> amazonReviewMention;
    private List<String> reviewMention;



}

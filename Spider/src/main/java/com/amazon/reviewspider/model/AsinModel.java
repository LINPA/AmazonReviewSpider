package com.amazon.reviewspider.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AsinModel {

    private Integer starLevelOne;
    private Integer starLevelTwo;
    private Integer starLevelThree;
    private Integer starLevelFour;
    private Integer starLevelFive;

    private String asin;

    private String img;

    private String brand;

    private String country;

}

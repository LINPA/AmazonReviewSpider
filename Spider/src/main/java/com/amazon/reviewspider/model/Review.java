package com.amazon.reviewspider.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Date;
import java.util.List;

@Document(indexName = "review_idx",type = "review")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Review {

    @Id
    private Long id;
    @Field(type = FieldType.Float)
    private Float starLevel;
    private String reviewTitle;
    private String buyer;
//    @Field(fielddata = true,type = FieldType.Date)
    private Long date;
    private String review;
    @Field(fielddata = true,type = FieldType.Auto)
    private String asin;
    private String videoUrl;
    private List<String> imageUrl;

}

package com.amazon.reviewspider.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ASINReviewModel {
    private String asin;
    private String star;
    private String terms;
    private Integer pageNumber = 0;
    private Integer type = 1;
}

package com.amazon.reviewspider.model;

import lombok.*;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Document(indexName = "asin_idx",type = "asin")
public class ASINHub {
    private Long id;
    @Field(fielddata = true,type = FieldType.Auto)
    private String asin;
    private String img;
    private String brand;
    private String country;
    private List<String> owner;
    private String url;
    @Field(type = FieldType.Float)
    private Float starLevel;
}

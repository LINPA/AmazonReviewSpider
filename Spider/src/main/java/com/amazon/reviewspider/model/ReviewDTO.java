package com.amazon.reviewspider.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ReviewDTO {

    private Float star;
    private String term;
    private Integer pageSize;
    private Integer pageNumber;
}

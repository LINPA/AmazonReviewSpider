package com.amazon.reviewspider.model;

import lombok.*;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.List;

@Document(indexName = "reviewmemtion_idx",type = "reviewmention")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ReviewMention {

    private Long id;
    private String asin;
    private List<String> word;
}

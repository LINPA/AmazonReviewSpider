package com.amazon.reviewspider.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.List;
import java.util.Set;

@Data
@Builder
@Document(indexName = "keyword_idx",type = "keyword")
@AllArgsConstructor
@NoArgsConstructor
public class KeyWordDIY {
    private Long id;
    private String asin;
    private Set<String> keyword;
}

package com.amazon.reviewspider.sample;

import com.hankcs.hanlp.HanLP;

import java.util.ArrayList;
import java.util.List;

public class HanLPSample {
    public static void main(String[] args) {
        //System.out.println(HanLP.segment("你好，欢迎使用HanLP汉语处理包！"));

        String content = "I have been using this remote control for about four years now. Back then, I paid about $120 for it. This has been one of my favorite stereo purchases. My wife found it confusing while we had six Sony remotes (2 VCRs, DVD player, amp, tape deck, cd player) that, to her, all looked similar. I bought the remote and it immediately worked flawlessly with all of the components. When I started replacing various equipment, all I had to do was reprogram the remote for the different components. For example, I now have a JVC TV. I entered the three digit code and it works perfectly. I also have a Cyberhome DVD player. I had to manually program all of the buttons. It took all of 5 minutes (including reading the instructions) and I now have full functionality of my DVD. No complaints. When this one dies (if it dies), I will not hesitate to replace it with another Sony universal remote.";
        List<String> keywordList = HanLP.extractKeyword(content,20);
        System.out.println("关键词提取 -> "+keywordList);


//        String document = "算法可大致分为基本算法、数据结构的算法、数论算法、计算几何的算法、图的算法、动态规划以及数值分析、加密算法、排序算法、检索算法、随机化算法、并行算法、厄米变形模型、随机森林算法。\n" +
//                "算法可以宽泛的分为三类，\n" +
//                "一，有限的确定性算法，这类算法在有限的一段时间内终止。他们可能要花很长时间来执行指定的任务，但仍将在一定的时间内终止。这类算法得出的结果常取决于输入值。\n" +
//                "二，有限的非确定算法，这类算法在有限的时间内终止。然而，对于一个（或一些）给定的数值，算法的结果并不是唯一的或确定的。\n" +
//                "三，无限的算法，是那些由于没有定义终止定义条件，或定义的条件无法由输入的数据满足而不终止运行的算法。通常，无限算法的产生是由于未能确定的定义终止条件。";
//        List<String> sentenceList = HanLP.extractSummary(content, 3);
//        System.out.println("自动摘要 ->"+sentenceList);


    }
}

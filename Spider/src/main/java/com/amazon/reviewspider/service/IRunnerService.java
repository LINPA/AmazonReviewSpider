package com.amazon.reviewspider.service;

import java.util.concurrent.Future;

public interface IRunnerService {

    Future<Boolean> run(String asin);

    void delete(String asin);

    Future<Boolean> cleanAndRun(String asin);
}

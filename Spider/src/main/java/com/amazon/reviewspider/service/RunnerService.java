package com.amazon.reviewspider.service;

import com.amazon.reviewspider.common.URLS;
import com.amazon.reviewspider.job.ReviewTask;
import com.amazon.reviewspider.model.Review;
import com.amazon.reviewspider.spider.pipeline.ASINHubPipeline;
import com.amazon.reviewspider.spider.pipeline.ESPipeline;
import com.amazon.reviewspider.spider.pipeline.ReviewMentionPipeline;
import com.amazon.reviewspider.spider.process.ASINHubProcess;
import com.amazon.reviewspider.spider.process.AmazonReviewProcess;
import com.amazon.reviewspider.spider.process.ReviewMentionProcess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import us.codecraft.webmagic.Spider;

import java.util.concurrent.Future;

@Service
@Transactional
public class RunnerService implements IRunnerService {

    @Qualifier("eSPipeline")
    @Autowired
    private ESPipeline esPipeline;

    @Qualifier("reviewMentionPipeline")
    @Autowired
    private ReviewMentionPipeline reviewMentionPipeline;

    @Qualifier("asinHubPipeline")
    @Autowired
    private ASINHubPipeline asinHubPipeline;

    @Autowired
    private ReviewService reviewService;

    @Autowired
    private ReviewTask reviewTask;

    @Autowired
    private ReviewMentionService reviewMentionService;

    @Autowired
    private KeyWordService keyWordService;

    @Autowired
    private ASINHubService asinHubService;

    @Override
    public Future<Boolean> run(String asin) {
        Future<Boolean> result = null;
        Page<Review> byASIN = reviewService.findByASIN(asin);
        if (byASIN.iterator().hasNext()) {
            String reviewUrl = String.format(URLS.AMAZON_REVIEW, asin);
            String reviewMentionUrl = String.format(URLS.AMAZON_REVIEW_MENTION, asin);
            String asinUrl = String.format(URLS.AMAZON_ASIN_DETAIL, asin);
            Spider.create(new ASINHubProcess(asin, "US"))
                    .addUrl(asinUrl)
                    .addPipeline(asinHubPipeline)
                    .thread(3)
                    .run();
            Spider.create(new ReviewMentionProcess(asin))
                    .addUrl(reviewMentionUrl)
                    .addPipeline(reviewMentionPipeline)
                    .thread(10)
                    .run();
            Spider.create(new AmazonReviewProcess(asin))
                    .addPipeline(esPipeline)
                    .addUrl(reviewUrl)
                    .thread(10)
                    .run();
            result = reviewTask.generateReviewExtractKeyword(asin);
        }
        return result;
    }

    @Override
    public void delete(String asin) {
        asinHubService.deleteByASIN(asin);
        reviewService.deleteByASIN(asin);
        reviewMentionService.deleteByASIN(asin);
        keyWordService.deleteByASIN(asin);
    }

    @Override
    public Future<Boolean> cleanAndRun(String asin) {
        delete(asin);
        return run(asin);
    }
}

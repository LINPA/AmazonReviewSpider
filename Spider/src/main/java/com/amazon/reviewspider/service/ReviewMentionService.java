package com.amazon.reviewspider.service;

import com.amazon.reviewspider.model.ReviewMention;
import com.amazon.reviewspider.repository.ReviewMentionRepository;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.DeleteQuery;
import org.springframework.stereotype.Service;

@Service
public class ReviewMentionService implements IReviewMentionService {

    @Autowired
    private ReviewMentionRepository reviewMentionRepository;
    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    @Override
    public ReviewMention save(ReviewMention reviewMention) {
        return reviewMentionRepository.save(reviewMention);
    }

    @Override
    public Iterable<ReviewMention> findByASIN(String asin) {
        MatchQueryBuilder matchQuery = QueryBuilders.matchQuery("asin", asin);
        return reviewMentionRepository.search(matchQuery);
    }

    @Override
    public void deleteByASIN(String asin) {
        DeleteQuery deleteQuery = new DeleteQuery();
        deleteQuery.setQuery(QueryBuilders.matchQuery("asin",asin));
        elasticsearchTemplate.delete(deleteQuery,ReviewMention.class);
    }
}

package com.amazon.reviewspider.service;

import com.amazon.reviewspider.model.KeyWordDIY;
import com.amazon.reviewspider.repository.KeyWordRepository;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.DeleteQuery;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.Optional;

@Service
public class KeyWordService implements IKeyWordService {

    @Autowired
    private KeyWordRepository keyWordRepository;

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    @Override
    public KeyWordDIY save(KeyWordDIY keyWordDIY) {
        return keyWordRepository.save(keyWordDIY);
    }

    @Override
    public Iterable<KeyWordDIY> save(Iterable<KeyWordDIY> keyWordDIYS) {
        return keyWordRepository.saveAll(keyWordDIYS);
    }

    @Override
    public Iterable<KeyWordDIY> getKeyWord(String asin) {
        MatchQueryBuilder matchQuery = QueryBuilders.matchQuery("asin", asin);
        return keyWordRepository.search(matchQuery);
    }

    @Override
    public void deleteByASIN(String asin) {
        DeleteQuery deleteQuery = new DeleteQuery();
        deleteQuery.setQuery(QueryBuilders.matchQuery("asin",asin));
        elasticsearchTemplate.delete(deleteQuery,KeyWordDIY.class);
    }
}

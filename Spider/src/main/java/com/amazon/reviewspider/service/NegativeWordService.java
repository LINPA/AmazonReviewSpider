package com.amazon.reviewspider.service;

import com.amazon.reviewspider.model.NegativeWord;
import com.amazon.reviewspider.repository.NegativeWordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.naming.NamingEnumeration;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class NegativeWordService implements INegativewordService {

    @Autowired
    private NegativeWordRepository negativeWordRepository;

    @Override
    public NegativeWord createNegativeWord(NegativeWord negativeWord) {
        return negativeWordRepository.save(negativeWord);
    }

    @Override
    public NegativeWord updateNegativeWord(List<String> words) {
        NegativeWord next = negativeWordRepository.findAll().iterator().next();
        Set<String> origin = next.getWords();
        origin.addAll(words);
        next.setWords(origin);
        negativeWordRepository.save(next);
        return next;
    }

    @Override
    public NegativeWord find() {
        return negativeWordRepository.findAll().iterator().next();
    }
}

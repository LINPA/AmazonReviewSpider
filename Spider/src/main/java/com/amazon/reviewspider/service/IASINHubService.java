package com.amazon.reviewspider.service;

import com.amazon.reviewspider.model.ASINHub;

import java.util.Iterator;

public interface IASINHubService {

    ASINHub save(ASINHub asinHub);

    Iterator<ASINHub> findByasin(String asin);

    Iterator<ASINHub> search(String term);

    void deleteByASIN(String asin);

}

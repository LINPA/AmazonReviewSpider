package com.amazon.reviewspider.service;

import com.amazon.reviewspider.model.KeyWordDIY;

import java.util.Iterator;
import java.util.Optional;

public interface IKeyWordService {

    KeyWordDIY save(KeyWordDIY keyWordDIY);

    Iterable<KeyWordDIY> save(Iterable<KeyWordDIY> keyWordDIYS);

    Iterable<KeyWordDIY> getKeyWord(String asin);

    void deleteByASIN(String asin);
}

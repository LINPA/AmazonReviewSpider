package com.amazon.reviewspider.service;

import com.amazon.reviewspider.common.SnowFlake;
import org.springframework.stereotype.Service;

@Service
public class IDGenerationService {

    private SnowFlake snowFlake = new SnowFlake(0,0);

    Long gererationID(){
        return snowFlake.nextId();
    }
}

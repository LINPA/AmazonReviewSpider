package com.amazon.reviewspider.service;

import com.amazon.reviewspider.model.ASINHub;
import com.amazon.reviewspider.repository.ASINHubRepository;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.DeleteQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Iterator;

@Service
@Transactional
public class ASINHubService implements IASINHubService{

    @Autowired
    private ASINHubRepository asinHubRepository;
    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    @Override
    public ASINHub save(ASINHub asinHub) {
        return asinHubRepository.save(asinHub);
    }

    @Override
    public Iterator<ASINHub> findByasin(String asin) {
        MatchQueryBuilder matchQuery = QueryBuilders.matchQuery("asin", asin);
        Iterable<ASINHub> search = asinHubRepository.search(matchQuery);
        return search.iterator();
    }

    @Override
    public Iterator<ASINHub> search(String term) {
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        boolQueryBuilder.should(QueryBuilders.wildcardQuery("asin.keyword",term));
        boolQueryBuilder.should(QueryBuilders.wildcardQuery("brand",term));
        return asinHubRepository.search(boolQueryBuilder).iterator();
    }

    @Override
    public void deleteByASIN(String asin) {
        DeleteQuery deleteQuery = new DeleteQuery();
        deleteQuery.setQuery(QueryBuilders.matchQuery("asin",asin));
        elasticsearchTemplate.delete(deleteQuery,ASINHub.class);
    }
}

package com.amazon.reviewspider.service;

import com.amazon.reviewspider.model.*;
import com.amazon.reviewspider.repository.ReviewRepository;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.*;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.sort.SortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.SearchResultMapper;
import org.springframework.data.elasticsearch.core.query.DeleteQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static org.elasticsearch.index.query.QueryBuilders.*;
import static org.elasticsearch.search.aggregations.AggregationBuilders.terms;

@Service
@Transactional
public class ReviewService implements IReviewService {

    // 分页参数
    Integer PAGE_SIZE = 25;//每页数量
    Integer DEFAULT_PAGE_NUMBER = 0;//默认当前页码
    //搜索模式
    String SCORE_MODE_SUM = "sum"; // 权重分求和模式
    Float  MIN_SCORE = 10.0F;      // 由于无相关性的分值默认为 1 ，设置权重分最小值为 10

    @Autowired
    private ReviewRepository reviewRepository;
    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;
    @Autowired
    private ReviewMentionService reviewMentionService;
    @Autowired
    private ASINHubService asinHubService;

    @Override
    public void createIndex(String asin) {

    }

    @Override
    public Iterable<Review> saveAll(Iterable<Review> reviews) {
        return reviewRepository.saveAll(reviews);
    }

    @Override
    public void saveOne(Review review) {
        reviewRepository.save(review);
    }

    @Override
    public Iterable<Review> getAllByASIN(String asin) {
        MatchQueryBuilder matchQuery = QueryBuilders.matchQuery("asin", asin);
        Iterable<Review> search = reviewRepository.search(matchQuery);
        return search;
    }


    @Override
    public Page<Review> findByASIN(String asin) {
        return reviewRepository.findAllByAsinEquals(asin,PageRequest.of(0,25));
    }

    @Override
    public Page<Review> findByKeyWorld(String searchWorld) {
        return reviewRepository.findAllByReviewContains(searchWorld,PageRequest.of(0,25));
    }

//    @Override
//    public List<Review> searchReview(Integer pageNumber, Integer pageSize, String searchContent) {
//        // 校验分页参数
//        if (pageSize == null || pageSize <= 0) {
//            pageSize = PAGE_SIZE;
//        }
//        if (pageNumber == null || pageNumber < DEFAULT_PAGE_NUMBER) {
//            pageNumber = DEFAULT_PAGE_NUMBER;
//        }
//        // 构建搜索查询
//        SearchQuery searchQuery = getReviewSearchQuery(pageNumber,pageSize,searchContent);
//        Page<Review> reviewPage = reviewRepository.search(searchQuery);
//        return reviewPage.getContent();
//    }
//
//    private SearchQuery getReviewSearchQuery(Integer pageNumber, Integer pageSize,String searchContent) {
//
//
//        FunctionScoreQueryBuilder functionScoreQueryBuilder = QueryBuilders.functionScoreQuery()
//                .add(QueryBuilders.matchPhraseQuery("review", searchContent),
//                        ScoreFunctionBuilders.weightFactorFunction(1000))
//                .add(QueryBuilders.matchPhraseQuery("reviewTitle", searchContent),
//                        ScoreFunctionBuilders.weightFactorFunction(500))
//                .scoreMode(SCORE_MODE_SUM).setMinScore(MIN_SCORE);
//        // 分页参数
//        Pageable pageable = PageRequest.of(pageNumber, pageSize);
//        return new NativeSearchQueryBuilder()
//                .withPageable(pageable)
//                .withQuery(functionScoreQueryBuilder).build();
//    }


    @Override
    public PageModel search(ASINReviewModel model) {
        if (model.getPageNumber() == null || model.getPageNumber() < DEFAULT_PAGE_NUMBER) {
            model.setPageNumber(DEFAULT_PAGE_NUMBER);
        }
        Pageable pageable =PageRequest.of(model.getPageNumber(), PAGE_SIZE);
        NativeSearchQueryBuilder nativeSearchQueryBuilder = new NativeSearchQueryBuilder();
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        if (model.getAsin() != null){
            boolQueryBuilder.must(matchQuery("asin",model.getAsin()));
        }
        if (null != model.getStar()){
            String starStr = model.getStar();
            System.out.println(starStr);
            if (starStr.contains("-")){
                String[] stars = starStr.split("-");
                System.out.println(stars[0]);
                System.out.println(stars[1]);
                boolQueryBuilder.must(rangeQuery("starLevel").gte(Float.valueOf(stars[0])).lte(stars[1]));
            }else {
                Float star = Float.valueOf(starStr);
                boolQueryBuilder.must(matchQuery("starLevel",star));
            }

            rangeQuery("starLevel").gte("").lte("");

        }
        if (null != model.getTerms()){
            boolQueryBuilder.must(multiMatchQuery(model.getTerms(),"reviewTitle","review"));
        }
        if (null != model.getType()){
            if (model.getType() == 2){
                BoolQueryBuilder typeQueryBuilder = QueryBuilders.boolQuery();
                typeQueryBuilder.should(existsQuery("imageUrl"));
                typeQueryBuilder.should(existsQuery("videoUrl"));
                boolQueryBuilder.must(typeQueryBuilder);
            }
        }
        SortBuilder sortBuilder = SortBuilders.fieldSort("date").order(SortOrder.DESC);

//        String preTag = "<font color='#dd4b39'>";//google的色值
//        String postTag = "</font>";
//        HighlightBuilder.Field  reviewTitleField = new HighlightBuilder.Field("reviewTitle").preTags(preTag).postTags(postTag);
//        HighlightBuilder.Field  reviewField = new HighlightBuilder.Field("reviewTitle").preTags(preTag).postTags(postTag);
        NativeSearchQuery query = nativeSearchQueryBuilder
                .withQuery(boolQueryBuilder)
                .withPageable(pageable)
                .withSort(sortBuilder)
               // .withHighlightFields(reviewField,reviewTitleField)
                .build();
        Page<Review> search = reviewRepository.search(query);
        return PageModel.builder()
                .content(search.getContent())
                .offset(search.getNumber())
                .limit(search.getSize())
                .total(search.getTotalElements())
                .build();
    }


    @Override
    public List<ReviewHubVO> findAllASINHub(String term,String who) {
        ArrayList<ReviewHubVO> reviewHubVOS= Lists.newArrayList();
        NativeSearchQueryBuilder nativeSearchQueryBuilder = new NativeSearchQueryBuilder()
                .withSearchType(SearchType.DEFAULT)
                .withTypes("asin")
                .addAggregation(terms("asin_group").field("asin"));
        if (!(StringUtils.isBlank(term)) && !(term.equals("none"))){
            BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
            boolQueryBuilder.should(QueryBuilders.wildcardQuery("asin.keyword","*"+term.toUpperCase()+"*"));
            boolQueryBuilder.should(QueryBuilders.wildcardQuery("brand","*"+term+"*"));
            //添加用户识别数据
            //boolQueryBuilder.must(QueryBuilders.matchPhraseQuery("owner",who));
            nativeSearchQueryBuilder.withQuery(boolQueryBuilder);
        }else {
            nativeSearchQueryBuilder.withQuery(matchAllQuery());
        }

        Aggregations aggregations = elasticsearchTemplate.query(nativeSearchQueryBuilder.build(), SearchResponse::getAggregations);
        Map<String, Aggregation> aggregationMap = aggregations.asMap();
        for (String s : aggregationMap.keySet()) {
            StringTerms stringTerms = (StringTerms) aggregationMap.get(s);
            List<StringTerms.Bucket> buckets = stringTerms.getBuckets();
            for (Terms.Bucket bucket : buckets) {
                String asin = bucket.getKeyAsString();
                ReviewHubVO reviewHubVO = searchASINStar(asin);
                Iterator<ASINHub> byasin = asinHubService.findByasin(asin);
                while (byasin.hasNext()){
                    ASINHub next = byasin.next();
                    reviewHubVO.setCountry(next.getCountry());
                    reviewHubVO.setBrand(next.getBrand());
                    reviewHubVO.setImg(next.getImg());
                }
                reviewHubVOS.add(reviewHubVO);
            }
        }
        return reviewHubVOS;
    }

    @Override
    public void deleteByASIN(String asin) {
        DeleteQuery deleteQuery = new DeleteQuery();
        deleteQuery.setQuery(matchQuery("asin",asin));
        elasticsearchTemplate.delete(deleteQuery,Review.class);
    }

    private  ReviewHubVO searchASINStar(String asin) {
        Long one = 0L, two = 0L, three = 0L, four = 0L, five = 0L;
        ReviewHubVO.ReviewHubVOBuilder hubVOBuilder = ReviewHubVO.builder();
        NativeSearchQuery starSearchQuery = new NativeSearchQueryBuilder()
                .withQuery(matchPhraseQuery("asin", asin))
                .withTypes("review")
                .addAggregation(terms("star_group").field("starLevel"))
                .build();
        Aggregations starAggregations = elasticsearchTemplate.query(starSearchQuery, SearchResponse::getAggregations);
        Map<String, Aggregation> starAggregationMap = starAggregations.asMap();
        for (String tmp : starAggregationMap.keySet()) {
            DoubleTerms starTerms = (DoubleTerms) starAggregationMap.get(tmp);
            Terms.Bucket oneBucket = starTerms.getBucketByKey("1.0");
            one = oneBucket.getDocCount();
            Terms.Bucket twoBucket = starTerms.getBucketByKey("2.0");
            two = twoBucket.getDocCount();
            Terms.Bucket threeBucket = starTerms.getBucketByKey("3.0");
            three = threeBucket.getDocCount();
            Terms.Bucket fourBucket = starTerms.getBucketByKey("4.0");
            four = fourBucket.getDocCount();
            Terms.Bucket fiveBucket = starTerms.getBucketByKey("5.0");
            five = fiveBucket.getDocCount();
            hubVOBuilder
                    .asin(asin)
                    .reviewCount(one+two+three+four+five)
                    .starLevelOne(one)
                    .starLevelTwo(two)
                    .starLevelThree(three)
                    .starLevelFour(four)
                    .starLevelFive(five);
        }
        Iterable<ReviewMention> reviewMentionIterable = reviewMentionService.findByASIN(asin);
        for (ReviewMention aReviewMentionIterable : reviewMentionIterable) {
            hubVOBuilder.amazonReviewMention(aReviewMentionIterable.getWord());
        }
        return hubVOBuilder.build();
    }

}

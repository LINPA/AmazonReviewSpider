package com.amazon.reviewspider.service;

import com.amazon.reviewspider.model.*;
import org.springframework.data.domain.Page;

import java.util.List;

public interface IReviewService {

    void createIndex(String asin);

    Iterable<Review> saveAll(Iterable<Review> reviews);

    void saveOne(Review review);

    Iterable<Review> getAllByASIN(String asin);

    Page<Review> findByASIN(String asin);

    Page<Review> findByKeyWorld(String searchWorld);

    // Integer pageNumber,String asin, Float star, String searchContent
    PageModel search(ASINReviewModel model);


//    public List<Review> searchReview(Integer pageNumber, Integer pageSize, String searchContent);

    List<ReviewHubVO> findAllASINHub(String term, String who);

    void deleteByASIN(String asin);

}

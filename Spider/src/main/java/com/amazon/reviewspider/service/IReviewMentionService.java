package com.amazon.reviewspider.service;

import com.amazon.reviewspider.model.ReviewMention;

public interface IReviewMentionService {

    ReviewMention save(ReviewMention reviewMention);

    Iterable<ReviewMention> findByASIN(String asin);

    void deleteByASIN(String asin);

}

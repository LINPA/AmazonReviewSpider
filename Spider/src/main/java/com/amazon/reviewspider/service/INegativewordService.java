package com.amazon.reviewspider.service;

import com.amazon.reviewspider.model.NegativeWord;

import java.util.List;
import java.util.Set;

public interface INegativewordService {

    NegativeWord createNegativeWord(NegativeWord negativeWord);

    NegativeWord updateNegativeWord(List<String> words);

    NegativeWord find();
}

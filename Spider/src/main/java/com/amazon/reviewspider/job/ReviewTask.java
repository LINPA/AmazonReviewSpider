package com.amazon.reviewspider.job;

import com.amazon.reviewspider.common.SnowFlake;
import com.amazon.reviewspider.model.KeyWordDIY;
import com.amazon.reviewspider.model.NegativeWord;
import com.amazon.reviewspider.model.Review;
import com.amazon.reviewspider.service.KeyWordService;
import com.amazon.reviewspider.service.NegativeWordService;
import com.amazon.reviewspider.service.ReviewService;
import com.google.common.collect.Lists;
import com.hankcs.hanlp.HanLP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.Future;

@Component
public class ReviewTask {

    private int maxRetryTimes = 3;
    private int retrySleepMillis = 10000;

    @Autowired
    private KeyWordService keyWordService;
    @Autowired
    private ReviewService reviewService;
    @Autowired
    private NegativeWordService negativeWordService;

    private SnowFlake snowFlake = new SnowFlake(2,0);

    @Async
    public Future<Boolean> generateReviewExtractKeyword(String asin){
        Iterable<KeyWordDIY> keyWord = keyWordService.getKeyWord(asin);
        Iterator<KeyWordDIY> iterator = keyWord.iterator();
        if (!iterator.hasNext()){
            //提取reviewtitle和review数据，生成关键词
            System.out.println("当前asin未生成过关键词 asin="+asin);
            StringBuffer stringBuffer = new StringBuffer();
            Iterable<Review> allByASIN = reviewService.getAllByASIN(asin);
            allByASIN.forEach(review -> stringBuffer.append(review.getReview()).append(" ").append(review.getReviewTitle()).append(" "));
            System.out.println("正在生成关键词 asin="+asin);
            List<String> keywordList = HanLP.extractKeyword(stringBuffer.toString(), 25);
            NegativeWord negativeWord = negativeWordService.find();
            Set<String> negativeWordWords = negativeWord.getWords();
            negativeWordWords.addAll(keywordList);
            KeyWordDIY keyWordDIY = KeyWordDIY.builder()
                    .asin(asin)
                    .id(snowFlake.nextId())
                    .keyword(negativeWordWords).build();
            keyWordService.save(keyWordDIY);
            System.out.println("关键词生成完成 asin="+asin);
            return new AsyncResult<>(Boolean.TRUE);
        }else {
            return new AsyncResult<>(Boolean.FALSE);
        }


//        int retryTimes = 0;
//        do {
//            System.out.println("生成关键词 asin="+asin);
//            //从类型为review的索引中查找该asin记录，提取review body到keyword索引存储
//            Iterable<KeyWordDIY> keyWord = keyWordService.getKeyWord(asin);
//            Iterator<KeyWordDIY> iterator = keyWord.iterator();
//            if (!iterator.hasNext()){
//                //提取reviewtitle和review数据，生成关键词
//                System.out.println("当前asin未生成过关键词 asin="+asin);
//                StringBuffer stringBuffer = new StringBuffer();
//                Iterable<Review> allByASIN = reviewService.getAllByASIN(asin);
//                allByASIN.forEach(review -> stringBuffer.append(review.getReview()).append(" ").append(review.getReviewTitle()));
//                System.out.println("正在生成关键词 asin="+asin);
//                List<String> keywordList = HanLP.extractKeyword(stringBuffer.toString(), 25);
//                KeyWordDIY keyWordDIY = KeyWordDIY.builder()
//                        .asin(asin)
//                        .id(snowFlake.nextId())
//                        .keyword(keywordList).build();
//                keyWordService.save(keyWordDIY);
//                System.out.println("关键词生成完成 asin="+asin);
//                return new AsyncResult<>(Boolean.TRUE);
//            }else {
//                if (retryTimes+1 > this.maxRetryTimes) return new AsyncResult<>(Boolean.FALSE);
//                // -1 系统繁忙, 1000ms后重试
//                int sleepMillis = this.retrySleepMillis * (1 << retryTimes);
//                try {
//                     System.out.println("该asin未抓取到review数据,"+sleepMillis+"后重试，第"+retryTimes+"次");
//                    Thread.sleep(sleepMillis);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//
//        }while (retryTimes++ < this.maxRetryTimes);
//        System.out.println("达到最大次数 asin="+asin);
    }
}

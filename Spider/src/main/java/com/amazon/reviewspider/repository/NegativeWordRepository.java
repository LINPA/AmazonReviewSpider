package com.amazon.reviewspider.repository;

import com.amazon.reviewspider.model.NegativeWord;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NegativeWordRepository extends ElasticsearchRepository<NegativeWord,Long> {

}

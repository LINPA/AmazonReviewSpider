package com.amazon.reviewspider.repository;

import com.amazon.reviewspider.model.Review;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReviewRepository extends ElasticsearchRepository<Review,Long> {


    Page<Review> findAllByAsinEquals(String asin, Pageable pageable);

    Page<Review> findAllByReviewContains(String searchWorld, Pageable pageable);

}

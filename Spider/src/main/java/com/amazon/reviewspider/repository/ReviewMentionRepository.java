package com.amazon.reviewspider.repository;

import com.amazon.reviewspider.model.ReviewMention;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReviewMentionRepository  extends ElasticsearchRepository<ReviewMention,Long> {
}

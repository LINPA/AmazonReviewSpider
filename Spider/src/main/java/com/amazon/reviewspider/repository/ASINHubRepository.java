package com.amazon.reviewspider.repository;

import com.amazon.reviewspider.model.ASINHub;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ASINHubRepository extends ElasticsearchRepository<ASINHub,Long>{
}

package com.amazon.reviewspider.repository;

import com.amazon.reviewspider.model.KeyWordDIY;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KeyWordRepository extends ElasticsearchRepository<KeyWordDIY,Long>{

    KeyWordDIY findByAsin(String asin);
}
